﻿using System;
using LibTicTacToe;
using System.Collections.Generic;

namespace test_app
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				using (TicTacToeLogic tlogic = new TicTacToeLogic(new char[3, 3]))
				{
					tlogic.TryFillSquares(2, "X");
					tlogic.TryFillSquares(3, "O");
				}
			}
			catch (ArgumentNullException ae)
			{
				Console.WriteLine("An ArgumentNullException was thrown at " + ae.StackTrace + "\n" + ae.Message);
			}
			catch (ArgumentException a)
			{
				Console.WriteLine("An ArgumentException thrown at " + a.StackTrace + "\n" + a.Message);
			}
			catch (Exception e)
			{
				Console.WriteLine("An Exception was thrown at " + e.StackTrace + "\n" + e.Message);
			}
		}
	}
}
