﻿#define Debug
#define Release

using System;
using System.Collections;
using System.Collections.Generic;

namespace LibTicTacToe
{
	public enum TicTacToeChar
	{
		X,
		O,
		NoChar
	}

	public class TicTacToeLogic:IDisposable
	{
		bool objectDisposed = false;

		char[,] matrix;

		int numberOfSquares, maxSquareNumber;

		Dictionary<int, Tuple<TicTacToeChar, Tuple<int, int>>> freeSquares, filledSquares;
		/*
			Read the specification as 
			<square number, <TicTacToeChar, <matrix index 1, matrix index 2>>>
		*/

#if Debug
		/// <summary>
		/// Fills a square
		/// </summary>
		/// <param name="square"></param>		
		/// <exception cref="ArgumentException"/>
		/// <exception cref="Exception"/>
		public bool TryFillSquares(int squareNumber, string tttChar)
		{
			if (squareNumber > maxSquareNumber)
				throw new ArgumentException("That square is not addressable");
			//Check if the square number is occupied
			if (!freeSquares.ContainsKey(squareNumber) && filledSquares.ContainsKey(squareNumber))
				throw new ArgumentException("That square is already filled");
			//If the square number number is free, obtain the value before removing it from the freeSquares dictionary
			Tuple<TicTacToeChar, Tuple<int, int>> val;
			if (!freeSquares.TryGetValue(squareNumber, out val))
				throw new ArgumentNullException("An argument was not recieved as expected");
			Tuple<int, int> squareAddress = val.Item2;
			TicTacToeChar tchar = Parse(tttChar);
			Tuple<TicTacToeChar, Tuple<int, int>> associatedValue = Tuple.Create<TicTacToeChar, Tuple<int, int>>(tchar, squareAddress);
			if (!freeSquares.Remove(squareNumber))
				throw new Exception("");
			filledSquares.Add(squareNumber, associatedValue);
			return true;
		}
#endif

#if Release
		/// <summary>
		/// An implementation of the Dispose pattern
		/// </summary>
		/// <param name="disposing"></param>
		/// <exception cref="ObjectDisposedException"/>
		protected virtual void Dispose(bool disposing)
		{
			if (objectDisposed)
				throw new ObjectDisposedException("The game is already over\n");
			if (disposing)
			{
				/*
					dispose of managed resources here
				*/
				freeSquares = null;
				filledSquares = null;
			}
			objectDisposed = true;
		}

		/// <summary>
		/// Disposes of the current object and releases any resources held by it.
		/// </summary>		
		/// <exception cref="ArgumentNullException"/>
		public TicTacToeLogic(char[,] tttMatrix)
		{
			if (tttMatrix == null)
				throw new ArgumentNullException("A null matrix cannot be used");
			matrix = tttMatrix;
			int lenRow = tttMatrix.GetLength(0), lenCol = tttMatrix.GetLength(1), n = 0;
			filledSquares = new Dictionary<int, Tuple<TicTacToeChar, Tuple<int, int>>>();
			freeSquares = new Dictionary<int, Tuple<TicTacToeChar, Tuple<int, int>>>();
			for (int i = 0; i < lenRow; i++)
			{
				for (int j = 0; j < lenCol; j++)
				{
					freeSquares.Add(n, Tuple.Create<TicTacToeChar, Tuple<int, int>>(TicTacToeChar.NoChar, Tuple.Create<int, int>(i, j)));
					n += 1;
				}
			}
			numberOfSquares = maxSquareNumber = n;
		}
		/// <summary>
		/// Lets-go of all the resources held by this object and takes it out of memory.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}		
		/// Given a string, parses it to its equivalent TicTacToeChar
		/// </summary>
		/// <param name="symbol"></param>
		/// <returns></returns>
		/// <exception cref="FormatException"/>
		public static TicTacToeChar Parse(string symbol)
		{
			TicTacToeChar tchar = TicTacToeChar.NoChar;
			if (symbol.Equals("X"))
				tchar = TicTacToeChar.X;
			else
			if (symbol.Equals("O"))
				tchar = TicTacToeChar.O;
			else
				throw new FormatException("The input seems to be in an incorrect format");
			return tchar;
		}
#endif
	}
}